# FreedomBox Infrastructure

Infrastructure as code for the FreedomBox continuous integration and build system.

### Components

- `ci`
  - Containing the CI resources for provisioning infrastructure to build new FreedomBox images for various hardware.
  - UI code for https://ci.freedombox.org


- `config`
  - Contains the BuildBot master configuration files to run the FreedomBox continuous integration and build system.


### Credentials storage

The BuildBot master expects credentials for user accounts and passwords for workers. These should be specified in JSON format on the build master's server at `/etc/buildbot/auth.json`.

Copy the file auth.json.sample to `/etc/buildbot/auth.json` and replace the sample data with the correct data.

The same password as provided in auth.json has to be provided in the buildbot.tac file of each worker.
