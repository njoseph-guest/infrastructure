#!/usr/bin/env python3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
Download status files for local development.
"""

import json
import os
import subprocess

base_path = 'https://ci.freedombox.org/status/'


def download(file_name):
    print('Downloading -', file_name)
    try:
        os.unlink(file_name)
    except FileNotFoundError:
        pass
    subprocess.run(['wget', '-q', '-nc', base_path + file_name])


# download('pipeline-info.json')
pipelines = json.loads(open('pipeline-info.json').read())
for group in pipelines:
    group_name = group['name'].split('-')[-1]
    for pipeline in group['pipelines']:
        download('{}-{}-status.json'.format(pipeline, group_name))
