#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from buildbot.plugins import *

from .builders import builders

# A single nightly scheduler for all the builders
# Runs at 1 am on Saturdays
# It is assumed that a release made on Tuesday will have migrated to testing by Saturday
weekly_schedulers = [
    schedulers.Nightly(name='weekly-testing', branch='master',
                       builderNames=[builder.name for builder in builders],
                       hour=1, minute=0, dayOfWeek=5)  # Monday is 0
]

# One force scheduler per builder configuration
force_schedulers = [
    schedulers.ForceScheduler(name="force-{}-build".format(builder.name),
                              builderNames=[builder.name])
    for builder in builders
]

schedulers = weekly_schedulers + force_schedulers
