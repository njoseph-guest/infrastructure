#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from buildbot.plugins import *

from ..common import *
from ..common.builders import (BuilderConfigFactory, BuildTarget,
                               get_build_targets)

distribution = 'unstable'
build_stamp = 'dailydebian'

build_targets = get_build_targets(distribution, build_stamp)

builders = [
    BuilderConfigFactory(build_target.target,
                         build_target.artifact_name).create()
    for build_target in build_targets
]
