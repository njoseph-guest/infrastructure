#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import subprocess

additional_dependencies = [
    'build-essential', 'python3-setuptools', 'python3-coverage', 'sshpass',
    'parted'
]


def get_plinth_dependencies():
    lines = subprocess.run(['apt-cache', 'depends', 'freedombox'],
                           stdout=subprocess.PIPE).stdout.decode().split('\n')
    apt_dependencies = []
    for line in lines:
        line = line.strip()
        if line.startswith('Depends') and '<' not in line:
            apt_dependencies.append(line.split(':')[1].strip())
    return apt_dependencies + additional_dependencies
