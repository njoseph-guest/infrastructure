#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from buildbot.plugins import *

from . import app_server, functional_tests, plinth
from ..common import *
from .builders import builders

# One force scheduler per builder configuration
force_schedulers = [
    schedulers.ForceScheduler(name="force-{}-build".format(builder.name),
                              builderNames=[builder.name])
    for builder in builders
]

virtualbox_scheduler = schedulers.Dependent(
    name='virtualbox-build', builderNames=['virtualbox-amd64-dev'],
    upstream=plinth.polling_scheduler)

app_server_scheduler = schedulers.Dependent(
    name='app-server-build', builderNames=[app_server.BUILDER_NAME],
    upstream=virtualbox_scheduler)

functional_tests_scheduler = schedulers.Dependent(
    name='functional-tests-build',
    builderNames=[functional_tests.BUILDER_NAME],
    upstream=app_server_scheduler)

dependent_schedulers = [
    virtualbox_scheduler, app_server_scheduler, functional_tests_scheduler
]

schedulers = plinth.schedulers + force_schedulers + dependent_schedulers

builders = plinth.builders + builders + app_server.builders + functional_tests.builders

change_sources = plinth.change_sources
