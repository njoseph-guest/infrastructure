#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from buildbot.plugins import *

from ..common import *
from .utils import get_plinth_dependencies

FREEDOMBOX_URL = "https://salsa.debian.org/freedombox-team/freedombox.git"

steps = [
    steps.Git(name="checkout source code", repourl=FREEDOMBOX_URL, mode='full',
              shallow=True, haltOnFailure=True),
    steps.ShellSequence(
        name="build debian package", commands=[
            util.ShellArg(command=["sudo", "apt", "build-dep", "-y", "."]),
            util.ShellArg(command=["gbp", "buildpackage", "--git-pbuilder"])
        ], env={"DEBIAN_FRONTEND": "noninteractive"}, haltOnFailure=True),
    # TODO Run tests as sudo
    # steps.ShellCommand(
    #     name="Set PYTHONPATH", command=
    #     'export PYTHONPATH=`{ echo $PWD & python3 -c "import sys; print(\':\'.join(sys.path))"; } | paste -d "" - -`',
    #     haltOnFailure=True),
    steps.ShellSequence(
        name="retrieve and rename build artifacts", commands=[
            util.ShellArg(
                command=["mv ../freedombox_*.deb freedombox_dev.deb"]),
            util.ShellArg(command=[
                "mv ../freedombox-doc-en*.deb freedombox-doc-en_dev.deb"
            ]),
            util.ShellArg(command=[
                "mv ../freedombox-doc-es*.deb freedombox-doc-es_dev.deb"
            ])
        ], haltOnFailure=True),
    steps.ShellCommand(name="rename to freedombox_dev.deb",
                       command='mv ../freedombox_*.deb freedombox_dev.deb',
                       haltOnFailure=True),
    steps.MultipleFileUpload(
        name="upload build artifacts to master", workersrcs=[
            "freedombox_dev.deb", "freedombox-doc-en_dev.deb",
            "freedombox-doc-es_dev.deb"
        ], masterdest="artifacts/")
]

BUILDER_NAME = 'plinth-dev'

builders = [
    util.BuilderConfig(name=BUILDER_NAME, workernames=["bilbo"],
                       factory=util.BuildFactory(steps))
]

polling_scheduler = schedulers.SingleBranchScheduler(
    name="plinth-scheduler",
    change_filter=util.ChangeFilter(project='plinth', branch='master'),
    treeStableTimer=None, builderNames=[BUILDER_NAME])

force_scheduler = schedulers.ForceScheduler(name="force-plinth-build",
                                            builderNames=[BUILDER_NAME])

schedulers = [polling_scheduler, force_scheduler]

change_sources = [
    changes.GitPoller(FREEDOMBOX_URL, workdir='plinth-git-poller',
                      project='plinth', branch='master', pollInterval=300)
]
