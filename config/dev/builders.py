#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from buildbot.plugins import *

from ..common import *
from ..common.builders import BuilderConfigFactory, BuildTarget

BUILD_STAMP = "dailyupstream"


class DevBuilderConfigFactory(BuilderConfigFactory):
    """Factory to create builder configuration for development builds."""
    def steps(self):
        return [
            steps.ShellCommand(name='cleanup build directory',
                               command=['sudo', 'rm', '-rf',
                                        'build'], haltOnFailure=True),
            steps.Git(name="checkout source code", repourl=FREEDOM_MAKER_URL,
                      mode='full', alwaysUseLatest=True, haltOnFailure=True),
            steps.FileDownload(name="download freedombox_dev.deb",
                               mastersrc="artifacts/freedombox_dev.deb",
                               workerdest="freedombox_dev.deb"),
            steps.FileDownload(name="download freedombox-doc-en_dev.deb",
                               mastersrc="artifacts/freedombox-doc-en_dev.deb",
                               workerdest="freedombox-doc-en_dev.deb"),
            steps.FileDownload(name="download freedombox-doc-es_dev.deb",
                               mastersrc="artifacts/freedombox-doc-es_dev.deb",
                               workerdest="freedombox-doc-es_dev.deb"),
            steps.ShellCommand(
                name='build FreedomBox image', command=[
                    'sudo', 'python3', '-B', '-m', 'freedommaker', '--force',
                    '--build-mirror={BUILD_MIRROR}',
                    '--custom-package=freedombox_dev.deb',
                    '--custom-package=freedombox-doc-en_dev.deb',
                    '--custom-package=freedombox-doc-es_dev.deb',
                    '--skip-compression', f'--build-stamp={BUILD_STAMP}',
                    '--build-in-ram', self.target
                ], haltOnFailure=True, timeout=3600),
            steps.ShellCommand(
                name='change ownership',
                command=['sudo', 'chown', '-R', 'buildbot:buildbot',
                         'build'], alwaysRun=True),
            steps.ShellCommand(name="change permissions",
                               command="chmod -R 644 build/*"),
            steps.FileUpload(name='upload the artifact to build master',
                             workersrc='build/' + self.artifact_name,
                             masterdest='artifacts/' + self.artifact_name),
        ]


build_targets = [
    BuildTarget(
        target='virtualbox-amd64',
        artifact_name=f'freedombox-unstable-free_{BUILD_STAMP}_all-amd64.vdi')
]

builders = [
    DevBuilderConfigFactory(build_target.target, build_target.artifact_name,
                            'dev').create() for build_target in build_targets
]
