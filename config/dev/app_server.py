#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from buildbot.plugins import *

BUILDER_NAME = 'app-server-dev'

spin_down_commands = [
    ('power off app server',
     ['VBoxManage', 'controlvm', 'app-server', 'poweroff']),
    ('remove attached storage',
     'VBoxManage storageattach app-server --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium none'
     ),
    ('close medium', [
        'VBoxManage', 'closemedium',
        '/var/lib/buildbot/workers/gimli/app-server-dev/build/freedombox-unstable-free_dailyupstream_all-amd64.vdi'
    ]),
    ('unregister vm', ['VBoxManage', 'unregistervm', 'app-server', '--delete'])
]

spin_down_steps = [
    steps.ShellCommand(name=name, command=cmd, haltOnFailure=False,
                       flunkOnFailure=False)
    for name, cmd in spin_down_commands
]

artifact_name = 'freedombox-unstable-free_dailyupstream_all-amd64.vdi.xz'

steps = spin_down_steps + [
    steps.FileDownload(name='download build artifact of virtualbox-amd64-dev',
                       mastersrc='artifacts/' + artifact_name,
                       workerdest=artifact_name),
    steps.ShellCommand(
        name="increase disk size to 8 GB", command=[
            'VBoxManage', 'modifymedium',
            'freedombox-unstable-free_dailyupstream_all-amd64.vdi', '--resize',
            '8000'
        ]),
    steps.FileDownload(name='download the spinup script',
                       mastersrc='scripts/spinup_test_vm.py',
                       workerdest='spinup_test_vm.py'),
    steps.ShellCommand(command=['python3', 'spinup_test_vm.py'],
                       haltOnFailure=True),
]

builders = [
    util.BuilderConfig(name=BUILDER_NAME, workernames=["gimli"],
                       factory=util.BuildFactory(steps))
]
