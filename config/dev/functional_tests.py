#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from buildbot.plugins import *

from .plinth import FREEDOMBOX_URL

BUILDER_NAME = 'functional-tests-dev'

steps = [
    steps.Git(name='fetch latest freedombox code', repourl=FREEDOMBOX_URL,
              mode='full', method='clobber', haltOnFailure=True),
    steps.ShellSequence(
        name='install dependencies', commands=[
            util.ShellArg(command=['sudo', 'apt', 'update']),
            util.ShellArg(command=[
                'sudo', 'DEBIAN_FRONTEND=noninteractive', 'apt', 'install',
                '-y', 'python3-pip', 'python3-pytest', 'xvfb',
                'python3-semver', 'virtualenv', 'virtualenvwrapper'
            ])
        ]),
    steps.FileDownload(
        name='Download virtualenv setup script',
        mastersrc='scripts/functional-tests/setup-virtualenv.sh',
        workerdest='setup-virtualenv.sh'),
    steps.ShellCommand(name='setup virtualenv',
                       command=['bash',
                                'setup-virtualenv.sh'], haltOnFailure=True),
    steps.FileDownload(name='Download installation script',
                       mastersrc='scripts/install_firefox_and_geckodriver.py',
                       workerdest='install_firefox_and_geckodriver.py'),
    steps.ShellCommand(
        name='install latest firefox and geckodriver',
        command=['python3', 'install_firefox_and_geckodriver.py']),
    steps.FileDownload(
        name='Download script to run functional tests',
        mastersrc='scripts/functional-tests/run-functional-tests.sh',
        workerdest='run-functional-tests.sh'),
    steps.ShellCommand(name='run functional tests',
                       command=['/bin/bash', 'run-functional-tests.sh'])
]

builders = [
    util.BuilderConfig(name=BUILDER_NAME, workernames=["bilbo"],
                       factory=util.BuildFactory(steps),
                       env={'FREEDOMBOX_URL': 'https://10.0.2.2:5430'})
]
