#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import json
import os

from twisted.application import internet
from twisted.internet import defer, task, utils
from twisted.python import log
from twisted.words.protocols import irc

from buildbot import config
from buildbot.process.results import *
from buildbot.reporters.http import HttpStatusPushBase
from buildbot.reporters.irc import IRC, IrcStatusBot, IrcStatusFactory
from buildbot.reporters.words import (Contact, StatusBot,
                                      ThrottledClientFactory)
from buildbot.util import service, ssl

from ..common import *


class FreedomBoxCIStatusPush(HttpStatusPushBase):
    name = "FreedomBoxCIStatusPush"

    def get_status(self, build):
        if build['results'] in [SUCCESS, WARNINGS]:
            return 'Passed'
        return 'Failed'

    @defer.inlineCallbacks
    def send(self, build):
        if build['complete']:
            status = dict()
            status['pipeline_name'] = build['builder']['name']
            status['result'] = self.get_status(build)
            status['time'] = build['complete_at'].timestamp()
            os.path.isdir('status') or os.mkdir('status')
            status_file_name = 'status/{}-status.json'.format(
                status['pipeline_name'])
            open(status_file_name, 'w').write(json.dumps(status))
            yield self.rsync(status_file_name)

    @defer.inlineCallbacks
    def rsync(self, status_file):
        """Copy the pipeline and result data to upload server."""
        target = '{}@{}:{}'.format(ARTIFACT_UPLOAD_USER, ARTIFACT_HOST,
                                   STATUS_FILE_UPLOAD_PATH)
        yield utils.getProcessValue('rsync', args=('--chmod=644', status_file,
                                                   target))


class LogUploader(HttpStatusPushBase):
    name = 'LogUploader'

    @defer.inlineCallbacks
    def send(self, build):
        # if build['complete']:
        steps = yield self.master.db.steps.getSteps(buildid=build['buildid'])
        steps = sorted(steps, key=lambda step: step['id'])
        full_log = []
        for step in steps:
            full_log.append('=== Step {} - {}\n'.format(
                step['number'], step['name']))
            full_log.append('=== Started at {}\n'.format(step['started_at']))
            logs = yield self.master.db.logs.getLogs(stepid=step['id'])
            for log_item in logs:
                log_data = yield self.master.db.logs.getLogLines(
                    log_item['id'], 0, log_item['num_lines'])
                full_log.append(log_data)

            full_log.append('=== Completed at {}\n\n'.format(
                step['complete_at']))

        full_log = ''.join(full_log)

        os.path.isdir('logs') or os.mkdir('logs')
        log_file_name = build['builder']['name'] + '.log'
        log_file_path = os.path.join('logs', log_file_name)
        open(log_file_path, 'w').write(full_log)
        yield utils.getProcessValue(
            'rsync', args=('--chmod=644', log_file_path, LOG_RSYNC_URL))


def getURLForBuild(builder_name):
    return 'https://ftp.freedombox.org/pub/freedombox/logs/' + builder_name + '.log'


class MyContact(Contact):
    @defer.inlineCallbacks
    def buildFinished(self, build):
        builder = yield self.getBuilder(builderid=build['builderid'])
        builderName = builder['name']
        buildNumber = build['number']
        buildResult = build['results']

        # only notify about builders we are interested in
        if (self.bot.tags is not None and
                not self.builderMatchesAnyTag(builder.get('tags', []))):
            log.msg('Not notifying for a build that does not match any tags')
            return

        if not (yield self.notify_for_finished(build)):
            return

        if not self.shouldReportBuild(builderName, buildNumber):
            return

        results = self.getResultsDescriptionAndColor(buildResult)

        if self.useRevisions:
            revisions = yield self.getRevisionsForBuild(build)
            r = "Build %s containing revision(s) [%s] is complete: %s" % \
                (builderName, ','.join(revisions), results[0])
        else:
            r = "Build %s #%d is complete: %s" % \
                (builderName, buildNumber, results[0])

        r += ' [%s]' % build['state_string']

        r += " - %s" % getURLForBuild(builderName)
        self.send(r)


class FreedomBoxIRCStatusBot(IrcStatusBot):
    contactClass = MyContact


class FreedomBoxIrcStatusFactory(IrcStatusFactory):
    protocol = FreedomBoxIRCStatusBot


class FreedomBoxIRC(IRC):
    name = "FreedomBoxIRC"
