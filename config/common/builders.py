#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from collections import namedtuple

from buildbot.plugins import *

from ..common import *

WORKER = 'legolas'

# Exclusive lock to run only one image build at a time on each worker.
build_lock = util.WorkerLock("image_builds", maxCount=1)

BuildTarget = namedtuple('BuildTarget', 'target artifact_name')


class BuilderConfigFactory():
    """Factory to produce a builder configuration."""

    def __init__(self, target, artifact_name, distribution='unstable',
                 build_stamp='dailydebian'):
        self.target = target
        self.artifact_name = artifact_name
        self.distribution = distribution
        self.build_stamp = build_stamp

    def builder_name(self):
        return self.target and self.target + '-' + self.distribution

    def _format_artifact_string(self, s):
        formatted = s.format(self.target)
        if self.distribution != 'unstable':
            formatted = formatted.replace('nightly', self.distribution)

        return formatted

    def get_artifact_upload_path(self):
        return self._format_artifact_string(ARTIFACT_UPLOAD_PATH)

    def get_artifact_rsync_url(self):
        return self._format_artifact_string(ARTIFACT_RSYNC_URL)

    def checkout_steps(self):
        return [
            steps.ShellCommand(name='cleanup build directory',
                               command=['sudo', 'rm', '-rf',
                                        'build'], haltOnFailure=True),
            steps.Git(name="checkout source code", repourl=FREEDOM_MAKER_URL,
                      mode='full', method='clobber', haltOnFailure=True)
        ]

    def build_steps(self):
        return [
            steps.ShellCommand(
                name='build FreedomBox image', command=[
                    'sudo', 'python3', '-B', '-m', 'freedommaker', '--force',
                    f'--build-mirror={BUILD_MIRROR}',
                    f'--build-stamp={self.build_stamp}',
                    f'--distribution={self.distribution}', '--build-in-ram',
                    self.target
                ], haltOnFailure=True, timeout=3600)
        ]

    def publish_steps(self):
        return [
            steps.ShellCommand(
                name='change ownership',
                command=['sudo', 'chown', '-R', 'buildbot:buildbot',
                         'build'], alwaysRun=True),
            steps.ShellCommand(
                name="sign the artifact", command=[
                    'gpg', '--batch', '--yes', '--detach-sign',
                    'build/' + self.artifact_name
                ], haltOnFailure=True),
            steps.ShellCommand(name="change permissions",
                               command="chmod -R 644 build/*"),
            steps.ShellCommand(
                name="create folder if needed",
                command=f'''sftp {ARTIFACT_UPLOAD_USER}@{ARTIFACT_HOST} <<EOF
                mkdir {UPLOAD_PATH}/hardware/{self.target}
                mkdir {self.get_artifact_upload_path()}
                EOF
                ''', haltOnFailure=True),
            steps.ShellCommand(
                name="upload artifact", command=[
                    'rsync', '-a', 'build/' + self.artifact_name,
                    self.get_artifact_rsync_url()
                ], timeout=1800, haltOnFailure=True),
            steps.ShellCommand(
                name="upload signature", command=[
                    'rsync', '-a', 'build/' + self.artifact_name + '.sig',
                    self.get_artifact_rsync_url()
                ]),
        ]

    def steps(self):
        return (
            self.checkout_steps() + self.build_steps() + self.publish_steps())

    def create(self):
        return util.BuilderConfig(name=self.builder_name(), workernames=[
            WORKER
        ], factory=util.BuildFactory(self.steps()),
                                  locks=[build_lock.access('exclusive')])


def get_build_targets(distribution, build_stamp):
    """Return a list of common build targets available in all distributions."""
    return [
        BuildTarget(
            target='a20-olinuxino-lime', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_a20-olinuxino-lime-armhf.img.xz'
        ),
        BuildTarget(
            target='a20-olinuxino-lime2', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_a20-olinuxino-lime2-armhf.img.xz'
        ),
        BuildTarget(
            target='a20-olinuxino-micro', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_a20-olinuxino-micro-armhf.img.xz'
        ),
        BuildTarget(
            target='amd64', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_all-amd64.img.xz'),
        BuildTarget(
            target='qemu-amd64', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_all-amd64.qcow2.xz'
        ),
        BuildTarget(
            target='virtualbox-amd64', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_all-amd64.vdi.xz'),
        BuildTarget(
            target='i386', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_all-i386.img.xz'),
        BuildTarget(
            target='qemu-i386', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_all-i386.qcow2.xz'),
        BuildTarget(
            target='virtualbox-i386', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_all-i386.vdi.xz'),
        BuildTarget(
            target='cubieboard2', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_cubieboard2-armhf.img.xz'
        ),
        BuildTarget(
            target='cubietruck', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_cubietruck-armhf.img.xz'
        ),
        BuildTarget(
            target='pcduino3', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_pcduino3-armhf.img.xz'
        ),
        BuildTarget(
            target='raspberry2', artifact_name=
            f'freedombox-{distribution}-nonfree_{build_stamp}_raspberry2-armhf.img.xz'
        ),
        BuildTarget(
            target='raspberry3', artifact_name=
            f'freedombox-{distribution}-nonfree_{build_stamp}_raspberry3-armhf.img.xz'
        ),
        BuildTarget(
            target='raspberry3-b-plus', artifact_name=
            f'freedombox-{distribution}-nonfree_{build_stamp}_raspberry3-b-plus-armhf.img.xz'
        ),
        BuildTarget(
            target='beaglebone', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_beaglebone-armhf.img.xz'
        ),
        BuildTarget(
            target='pine64-plus', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_pine64-plus-arm64.img.xz'
        ),
        BuildTarget(
            target='banana-pro', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_banana-pro-armhf.img.xz'
        ),
        BuildTarget(
            target='lamobo-r1', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_lamobo-r1-armhf.img.xz'
        ),
        BuildTarget(
            target='pine64-lts', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_pine64-lts-arm64.img.xz'
        ),
        BuildTarget(
            target='orange-pi-zero', artifact_name=
            f'freedombox-{distribution}-free_{build_stamp}_orange-pi-zero-armhf.img.xz'
        ),
    ]
