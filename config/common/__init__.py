#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from buildbot.plugins import changes

BUILD_MIRROR = "http://localhost:3142/debian"
ARTIFACT_UPLOAD_USER = 'dailybuilder'
ARTIFACT_HOST = 'fbfvm1.freedomboxfoundation.org'
UPLOAD_PATH = '/srv/ftp/pub/freedombox/'
ARTIFACT_UPLOAD_PATH = UPLOAD_PATH + 'hardware/{}/nightly/'
DEB_UPLOAD_PATH = UPLOAD_PATH + 'nightly/'
PIONEER_UPLOAD_PATH = UPLOAD_PATH + 'pioneer/'
LOG_UPLOAD_PATH = UPLOAD_PATH + 'logs/'
STATUS_FILE_UPLOAD_PATH = "/var/www/ci.freedombox.org/status/"
ARTIFACT_RSYNC_URL = ARTIFACT_UPLOAD_USER + '@' + ARTIFACT_HOST + ':' + ARTIFACT_UPLOAD_PATH
DEB_RSYNC_URL = ARTIFACT_UPLOAD_USER + '@' + ARTIFACT_HOST + ':' + DEB_UPLOAD_PATH
PIONEER_RSYNC_URL = ARTIFACT_UPLOAD_USER + '@' + ARTIFACT_HOST + ':' + PIONEER_UPLOAD_PATH
LOG_RSYNC_URL = ARTIFACT_UPLOAD_USER + '@' + ARTIFACT_HOST + ':' + LOG_UPLOAD_PATH
FREEDOM_MAKER_URL = "https://salsa.debian.org/freedombox-team/freedom-maker.git"

change_sources = [
    changes.GitPoller(FREEDOM_MAKER_URL, workdir='freedom-maker-git-poller',
                      project='freedom-maker', branch='master',
                      pollInterval=300),
]
