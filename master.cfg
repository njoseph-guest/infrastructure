# -*- python -*-
# ex: set filetype=python:

#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import json

from buildbot.plugins import *
from config import common, dev, pioneer, stable, testing, unstable
from config.common.reporters import (FreedomBoxCIStatusPush, FreedomBoxIRC,
                                     LogUploader)

# This is a sample buildmaster config file. It must be installed as
# 'master.cfg' in your buildmaster's base directory.

# This is the dictionary that the buildmaster pays attention to. We also use
# a shorter alias to save typing.
c = BuildmasterConfig = {}

auth = json.loads(open('/etc/buildbot/auth.json').read())

####### WORKERS

# The 'workers' list defines the set of recognized workers. Each element is
# a Worker object, specifying a unique worker name and password.  The same
# worker name and password must be configured on the worker.
c['workers'] = [
    worker.Worker(username, password)
    for username, password in auth['workers'].items()
]

# 'protocols' contains information about protocols which master will use for
# communicating with workers. You must define at least 'port' option that
# workers could connect to your master with this protocol.
# 'port' must match the value configured into the workers (with their
# --master option)
c['protocols'] = {'pb': {'port': 9989}}

####### CHANGESOURCES

# the 'change_source' setting tells the buildmaster how it should find out
# about source code changes.

c['change_source'] = dev.change_sources + common.change_sources

####### SCHEDULERS

# Configure the Schedulers, which decide how to react to incoming changes.

c['schedulers'] = dev.schedulers + pioneer.schedulers + stable.schedulers + \
    testing.schedulers + unstable.schedulers

####### BUILDERS

# The 'builders' list defines the Builders, which tell Buildbot how to perform a build:
# what steps, and which workers can execute them.  Note that any particular build will
# only take place on one worker.

c['builders'] = dev.builders + pioneer.builders + stable.builders + \
    testing.builders + unstable.builders

####### BUILDBOT SERVICES

# 'services' is a list of BuildbotService items like reporter targets. The
# status of each build will be pushed to these targets. buildbot/reporters/*.py
# has a variety to choose from, like IRC bots.

irc = FreedomBoxIRC(
    "irc.oftc.net", "buildbot", useSSL=True, useColors=False, port=6697,
    allowForce=True, channels=[{
        "channel": "#freedombox-ci"
    }], notify_events={
        'started': 1,
        'finished': 1,
        'success': 1,
        'failure': 1,
        'exception': 1,
        'successToFailure': 1,
        'failureToSuccess': 1,
    })

http = FreedomBoxCIStatusPush()
logs = LogUploader()

c['services'] = [irc, http, logs]

####### PROJECT IDENTITY

# the 'title' string will appear at the top of this buildbot installation's
# home pages (linked to the 'titleURL').

c['title'] = "FreedomBox CI"
c['titleURL'] = "https://ci.freedombox.org/"

# the 'buildbotURL' string should point to the location where the buildbot's
# internal web server is visible. This typically uses the port number set in
# the 'www' entry below, but with an externally-visible host name which the
# buildbot cannot figure out without some help.

c['buildbotURL'] = "http://10.136.2.140:8010/"

# minimalistic config to activate new web UI
c['www'] = dict(port=8010, plugins=dict(waterfall_view={}, console_view={},
                                        grid_view={}))
c['www']['authz'] = util.Authz(
    allowRules=[util.AnyEndpointMatcher(role="admins")], roleMatchers=[
        util.RolesFromUsername(roles=['admins'], usernames=['admin'])
    ])

c['www']['auth'] = util.UserPasswordAuth(auth['users'])

####### DB URL

c['db'] = {
    # This specifies what database buildbot uses to store its state. You can
    # leave this at its default for all but the largest installations.
    'db_url': "sqlite:///state.sqlite",
}

c['buildbotNetUsageData'] = None
