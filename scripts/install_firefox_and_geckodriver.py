#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import re
import subprocess

import semver

FIREFOX_VERSION = '60.0.2'
GECKODRIVER_VERSION = '0.20.1'

SEMVER_REGEX = re.compile(r'[0-9]+\.[0-9]+\.[0-9]+')


def get_version(package):
    output = subprocess.run(['/usr/local/bin/{}'.format(package), '--version'],
                            stderr=subprocess.DEVNULL, stdout=subprocess.PIPE)
    output = output.stdout.decode()
    return SEMVER_REGEX.findall(output)[0]


def install_firefox():
    subprocess.check_call([
        'wget', '-q',
        'https://download-installer.cdn.mozilla.net/pub/firefox/releases/{0}/linux-x86_64/en-US/firefox-{0}.tar.bz2'.
        format(FIREFOX_VERSION)
    ])
    subprocess.check_call([
        'sudo', 'tar', 'jxf', 'firefox-{}.tar.bz2'.format(FIREFOX_VERSION),
        '-C', '/var/lib'
    ]),
    subprocess.check_call(
        ['sudo', 'ln', '-s', '/var/lib/firefox/firefox', '/usr/local/bin/'])


def install_geckodriver():
    subprocess.check_call([
        'wget', '-q',
        'https://github.com/mozilla/geckodriver/releases/download/v{0}/geckodriver-v{0}-linux64.tar.gz'.
        format(GECKODRIVER_VERSION)
    ]),
    subprocess.check_call([
        'sudo', 'tar', 'xzf',
        'geckodriver-v{}-linux64.tar.gz'.format(GECKODRIVER_VERSION), '-C',
        '/usr/local/bin/'
    ])


if os.path.isfile('/usr/local/bin/firefox'):
    version = get_version('firefox')
    if semver.compare(version, FIREFOX_VERSION) == -1:
        install_firefox()
else:
    install_firefox()

if os.path.isfile('/usr/local/bin/geckodriver'):
    version = get_version('geckodriver')
    if semver.compare(version, GECKODRIVER_VERSION) == -1:
        install_geckodriver()
else:
    install_geckodriver()
