#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#!/usr/bin/python3

import subprocess
import time

script = '''
set -xe

VBoxManage createvm --name "app-server" --register
VBoxManage modifyvm app-server --cpus 4
VBoxManage modifyvm app-server --memory 2048
VBoxManage modifyvm app-server --ostype Debian_64
VBoxManage modifyvm app-server --vram 16
VBoxManage storagectl app-server --name "SATA Controller" --add sata --controller IntelAHCI
VBoxManage storageattach app-server --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium freedombox-unstable-free_dailyupstream_all-amd64.vdi

# Networking
VBoxManage modifyvm app-server --nic1 nat --nictype1 82545EM --natpf1 'HTTPS,tcp,,5430,,443' --natpf2 'SSH,tcp,,3222,,22'

# Start server
( nohup VBoxHeadless --startvm app-server >/dev/null 2>/dev/null & )
'''

subprocess.run(['bash'], input=script.encode(), check=True)

success = False
for _ in range(30):
    try:
        subprocess.run([
            'curl', '--silent', '-k', '--fail', 'https://localhost:5430/plinth'
        ], check=True)
        success = True
        break
    except subprocess.CalledProcessError as error:
        print('Failed to connect.', error)
        pass

    time.sleep(10)

if not success:
    print('Failed to bring up Plinth in VM.')
    raise SystemExit(-1)
