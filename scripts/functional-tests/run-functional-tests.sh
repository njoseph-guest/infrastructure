#!/bin/bash

source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
workon functional-tests
cd functional_tests
py.test-3 --include-functional -vv
