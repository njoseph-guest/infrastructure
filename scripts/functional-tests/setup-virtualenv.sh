#!/bin/bash

mkdir -p ~/.virtualenvs
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
# The following command also activates the virtualenv
mkvirtualenv --python=/usr/bin/python3 functional-tests
pip3 install -U splinter pytest-splinter pytest-bdd pytest-xvfb
